select * from _timescaledb_catalog.hypertable where table_name = 'ts';

SELECT distinct total_size FROM chunk_relation_size_pretty('ts') order by 1 desc;

SELECT count(*) FROM chunk_relation_size_pretty('ts');


SELECT chunk_id, chunk_table, ranges, table_size, index_size, toast_size, total_size
FROM chunk_relation_size_pretty('ts')
where total_size != '152 kB';

SELECT count(*) FROM ts;

SELECT * FROM ts
order by abs_date_time desc
limit 2;