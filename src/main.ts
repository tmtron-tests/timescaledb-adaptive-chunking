import * as inquirer from 'inquirer';
import { Db, initDb } from './db';
import { Question } from 'inquirer';
import chalk from 'chalk';
import * as ora from 'ora';
import { DataGenerator, TIncDateTimeFct } from './data-generator';

interface IRecreateAnswer {
  recreate: boolean;
}

const recreateQuestion: Question<IRecreateAnswer> = {
  type: 'confirm',
  name: 'recreate',
  message: 'Do you want to recreate the table (DROP/CREATE)?',
  default: true
};

interface IDataConfigAnswers {
  noOfBatches: number;
  batchSize: number;
  everyXMinutes: number;
}

const dataConfigQuestions: ReadonlyArray<Question<IDataConfigAnswers>> = [
  {
    type: 'input',
    name: 'noOfBatches',
    message: 'Number of batches to execute?',
    default: 1000
  },
  {
    type: 'input',
    name: 'batchSize',
    message: 'Number of rows per batch?',
    default: 5000
  },
  {
    type: 'input',
    name: 'everyXMinutes',
    message: 'One row every x minutes?',
    default: 1
  }
];

const HYPER_TABLE_NAME = 'ts';

async function initDatabaseTable(db: Db) {
  let recreateTable = true;
  const tableExists = await db.hyperTableExists(HYPER_TABLE_NAME);
  if (tableExists) {
    const answer = await inquirer.prompt(recreateQuestion);
    recreateTable = answer.recreate;
    if (recreateTable) {
      console.log(chalk.red('dropping table'));
      await db.dropTableIfExists(HYPER_TABLE_NAME);
    }
  }

  if (recreateTable) {
    console.log(chalk.blue('creating table'));
    await db.createHyperTable(HYPER_TABLE_NAME);
    console.log('table created');
  }
}

const START_DATE_TIME: Date = new Date(2013, 11, 14, 15, 5);

function incDateTimeByxMinutes(noOfMinutes: number): TIncDateTimeFct {
  return (dateTime: Date) => {
    const result = new Date(dateTime);
    result.setMinutes(dateTime.getMinutes() + noOfMinutes);
    return result;
  };
}

async function insertData(db: Db, answers: IDataConfigAnswers) {
  const dataGen = new DataGenerator(db, HYPER_TABLE_NAME, incDateTimeByxMinutes(answers.everyXMinutes), START_DATE_TIME);
  for (let batchIndex = 0; batchIndex < answers.noOfBatches; batchIndex++) {
    console.log('batch number' + batchIndex);
    await dataGen.insertTestData(answers.batchSize);
  }
}

async function main() {
  try {
    console.log('starting application');

    const spinnerInitDb = ora('initializing database').start();
    const db = await initDb();
    spinnerInitDb.succeed('database initialized');

    await initDatabaseTable(db);
    const answers = await inquirer.prompt(dataConfigQuestions);
    await insertData(db, answers);

    db.finishApp();
    console.log('finished');
  } catch (e) {
    console.log('error:', e);
  }
}

// noinspection JSIgnoredPromiseFromCall
main();
