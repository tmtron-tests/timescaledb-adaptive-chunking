import { ColumnSet, IDatabase, IMain, ITask } from 'pg-promise';
import * as pgPromise from 'pg-promise';
import { notEqualCaseInsensitive } from './data-generator';

const DATE_TIME_COL = 'abs_date_time';

export interface ITableMetaData {
  column_name: string;
  data_type: string;
}

export class Db {
  constructor(private readonly pgp: pgPromise.IMain, private readonly database: pgPromise.IDatabase<any>) {}

  async hyperTableExists(tableName: string) {
    // noinspection SqlResolve
    const result = await this.database.oneOrNone('select * from pg_catalog.pg_tables  where tablename = $1;', tableName);
    return !!result;
  }

  async dropTableIfExists(tableName: string) {
    // noinspection SyntaxError
    return this.database.none('DROP TABLE if exists $1:name', tableName);
  }

  async createHyperTable(tableName: string) {
    return this.database.tx(async (transaction: ITask<any>) => {
      const queries = [];
      queries.push(
        transaction.none('CREATE TABLE ${tableName:name} (${dateTimeCol:name} timestamp primary key, dset bigint, t1 real)', {
          tableName,
          dateTimeCol: DATE_TIME_COL
        })
      );
      queries.push(
        transaction.any("SELECT create_hypertable(${tableName}, ${dateTimeCol}, chunk_target_size => 'estimate')", {
          tableName,
          dateTimeCol: DATE_TIME_COL
        })
      );
      return transaction.batch(queries);
    });
  }

  async readMaxColValue(tableName: string) {
    return this.database.oneOrNone(
      `SELECT * 
          FROM $1:alias
          order by abs_date_time desc 
          limit 1
      `,
      tableName
    );
  }

  async readTableMetaData(tableName: string) {
    // noinspection SqlResolve
    const result: ITableMetaData[] = await this.database.many(
      `SELECT column_name, data_type
          FROM information_schema.columns
          WHERE table_schema = 'public'
          AND table_name = $1
      `,
      tableName
    );
    if (notEqualCaseInsensitive(result[0].column_name, 'abs_date_time')) throw Error('abs_date_time col must be first!');
    return result;
  }

  async batchInsert(columnSet: ColumnSet, data: Array<object>) {
    const insertStatement = this.pgp.helpers.insert(data, columnSet);
    return this.database.none(insertStatement);
  }

  createColumnSet(tableName: string, columnNameArr: string[]) {
    return new this.pgp.helpers.ColumnSet(columnNameArr, { table: tableName });
  }

  finishApp() {
    this.pgp.end();
  }
}

export async function initDb() {
  const postgresConfig = {
    host: 'localhost',
    port: 5432,
    database: 'vb',
    user: 'vb',
    password: 'vb'
  };
  const pgpMain: IMain = await pgPromise({
    query(e) {
      console.log(e.query);
    }
  });
  const database: IDatabase<any> = await pgpMain(postgresConfig);

  return new Db(pgpMain, database);
}
