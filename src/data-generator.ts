import { ColumnSet } from 'pg-promise';
import { Db, ITableMetaData } from './db';

export type TIncDateTimeFct = (dateTime: Date) => Date;

function equalCaseInsensitive(a: string | undefined, b: string | undefined): boolean {
  if (a && b) {
    return a.toUpperCase() === b.toUpperCase();
  }
  return false;
}
export function notEqualCaseInsensitive(a: string, b: string): boolean {
  return !equalCaseInsensitive(a, b);
}

abstract class ColumnBase {
  protected constructor(public readonly columnName: string) {}

  abstract getNextValue(dateTime: Date): any;
}

class ColumnInt extends ColumnBase {
  private previousValue: number;

  constructor(columnName: string, private readonly increaseBy: number, maxValue: number | undefined) {
    super(columnName);
    this.previousValue = maxValue || 0;
  }

  getNextValue(dateTime: Date): number {
    this.previousValue += this.increaseBy;
    return this.previousValue;
  }
}

class ColumnFloat extends ColumnBase {
  private previousValue: number;

  constructor(columnName: string, private readonly increaseBy: number, maxValue: number | undefined) {
    super(columnName);
    this.previousValue = maxValue || 1.01;
  }

  getNextValue(dateTime: Date): number {
    this.previousValue += this.increaseBy;
    return this.previousValue;
  }
}

const DummyStatusValues = ['off', 'starting', 'error'];

class ColumnString extends ColumnBase {
  private counter = 0;

  constructor(columnName: string, maxValue: string | undefined) {
    super(columnName);
  }

  getNextValue(dateTime: Date): string {
    this.counter++;
    return DummyStatusValues[this.counter % DummyStatusValues.length];
  }
}

class ColumnDateTime extends ColumnBase {
  private dateTime: Date;

  constructor(columnName: string, private readonly incDateTimeFct: TIncDateTimeFct, maxValue: Date) {
    super(columnName);
    this.dateTime = maxValue;
  }

  getNextDateTimeValue(): Date {
    return this.incDateTimeFct(this.dateTime);
  }

  getNextValue(dateTime: Date): string {
    this.dateTime = this.incDateTimeFct(this.dateTime);
    // NOTE: make sure to send UTC to the database - otherwise we will get primary key errors when the DLST changes!
    return this.dateTime.toUTCString();
  }
}

class RowDefinition {
  private dateTimeCol: ColumnDateTime;
  private readonly columnSet: ColumnSet;

  constructor(tableName: string, private readonly columns: ColumnBase[], private readonly database: Db) {
    this.dateTimeCol = columns[0] as ColumnDateTime;
    const columnNameArr = columns.map(colInfo => colInfo.columnName);
    this.columnSet = database.createColumnSet(tableName, columnNameArr);
  }

  private getRowValuesObject(dateTime: Date): object {
    const nextDateTime = this.dateTimeCol.getNextDateTimeValue();
    const result: {
      [index: string]: any;
    } = {};
    this.columns.forEach(col => (result[col.columnName] = col.getNextValue(nextDateTime)));
    return result;
  }

  async insertTestData(batchSize: number) {
    const data = new Array<object>(batchSize);
    for (let i = 0; i < batchSize; i++) {
      const dateTime = this.dateTimeCol.getNextDateTimeValue();
      data[i] = this.getRowValuesObject(dateTime);
    }

    return this.database.batchInsert(this.columnSet, data);
  }
}

export class DataGenerator {
  private rowDefinition: RowDefinition | undefined;

  constructor(
    private readonly database: Db,
    private readonly tableName: string,
    private readonly incDateTimeFct: TIncDateTimeFct,
    private readonly startDateTime: Date
  ) {}

  private buildColumn(metaData: ITableMetaData, maxValue: any): ColumnBase {
    if (metaData.data_type.includes('int')) {
      // note: bigint will be a string type - thus we must use Number(maxValue)
      return new ColumnInt(metaData.column_name, 1, Number(maxValue));
    } else if (metaData.data_type.includes('text')) {
      return new ColumnString(metaData.column_name, maxValue);
    } else if (metaData.data_type.includes('timestamp')) {
      return new ColumnDateTime(metaData.column_name, this.incDateTimeFct, maxValue || this.startDateTime);
    } else if (metaData.data_type.includes('double') || metaData.data_type.includes('real')) {
      return new ColumnFloat(metaData.column_name, 1.01, maxValue);
    }

    throw new Error('unknown data_type: ' + metaData.data_type);
  }

  private async getRowDefinition() {
    if (!this.rowDefinition) {
      const tableMetaData = await this.database.readTableMetaData(this.tableName);
      const maxColumnValues = await this.database.readMaxColValue(this.tableName);
      const columns = tableMetaData.map(metaData => {
        let maxValue: any | undefined;
        if (maxColumnValues) {
          maxValue = maxColumnValues[metaData.column_name];
        }
        return this.buildColumn(metaData, maxValue);
      });
      this.rowDefinition = new RowDefinition(this.tableName, columns, this.database);
    }
    return this.rowDefinition;
  }

  async insertTestData(batchSize: number) {
    const rowDef = await this.getRowDefinition();
    await rowDef.insertTestData(batchSize);
  }
}
